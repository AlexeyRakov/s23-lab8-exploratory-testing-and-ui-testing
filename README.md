# Lab 8 - Exploratory testing and UI

## Homework

For this lab I choose [dtf.ru](https://dtf.ru) website.

### **1. Checking license agreement**
| Action  | Status |
| ------------- | ------------- |
| Click "enter" button  | CHECK  |
| Wait opening popup  | CHECK  |
| Click "terms of use" link | CHECK |
| Wait opening page with "terms of use" | CHECK |
| Check title of page - should "Правила пользования сайтом DTF" | CHECK |

### **2. Checking "DTF Rate" page for all time**
| Action  | Status |
| ------------- | ------------- |
| Click "DTF Rate" button on left part of page  | CHECK  |
| Wait opening page  | CHECK  |
| Click button "For all time" on top of page  | CHECK |
| Wait opening page | CHECK |
| Check title of first entry - "Милые животные" | CHECK |

### **3. Checking opening login popup**
| Action  | Status |
| ------------- | ------------- |
| Click on "like" button on first article entry | CHECK  |
| Wait opening login popup  | CHECK  |
| Check if there is "Google" OAuth button  | CHECK |