from selenium import webdriver
from selenium.webdriver.common.by import By

options = webdriver.ChromeOptions()
options.add_argument("--headless")
driver = webdriver.Remote("http://selenium__standalone-chrome:4444/wd/hub", options=options)
driver.implicitly_wait(1)  # seconds

# implementation of test case 3 from README.md
driver.get("https://dtf.ru")

elem = driver.find_element(By.CSS_SELECTOR, "#page_wrapper > div > div.feed > div.feed__container > div > div:nth-child(1) > div > div.content-footer.content-footer--short.l-island-a > div:nth-child(1) > button")
elem.click()

elem = driver.find_element(By.CSS_SELECTOR, "body > div.v-popup-container > div > div > div.v-popup-window__content > div.auth > div.auth-social.auth__view.v-form > div.v-form__content > div > button.v-button.v-button--default.v-button--size-default.auth-social__button.auth-social__button--gg.auth-social__button--with-label")
social = elem.get_attribute("data-social")
assert social == "gg"

driver.close()
